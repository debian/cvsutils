#! /bin/sh

# This script is part of CVS Utilities, version @VERSION@.
# This script purges all unknown files, directories and softlinks,
# including ignored files.

echo "cvspurge: cleaning up ..."
cvsu --ignore --find --types '?LD' --batch="rm -rf" "$@"
test $? -eq 0 || exit 1
echo "cvspurge: done"
