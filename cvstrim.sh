#! /bin/sh

# This script is part of CVS Utilities, version @VERSION@.
# This script removes unknown files and directories except those that
# CVS would ignore.

set -e

echo "cvstrim: cleaning up ..."
cvsu --find --types '?D' --batch "rm -rf" "$@"

echo "cvstrim: done"
